import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Navbar from './components/Navbar'
import Home from './components/Home'
import Cart from './components/Cart'
import Signin from './components/sign-in'
import Thankyou from './components/thank-you.js'
import Sidebar from './components/sidebar.js'
import Tablet from './components/tablet.js'
import Tele from './components/tv.js'
import './App.css'

const items = [
  { name: 'home', label: 'Home', url: '/' },
  { name: 'phones', label: 'Phones', url:'/' },
  { name: 'tablets', label: 'Tablets', url:'/tablet' },
  { name: 'tvs', label: 'TVs', url:'/tv' }]



class App extends Component {
  render() {
    return (
       <BrowserRouter>
            <div className="App">
            
              <Navbar/>
              <Sidebar items={items} />
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="/cart" component={Cart}/>
                    <Route path="/signin" component={Signin}/>
                    <Route path="/thankyou" component={Thankyou}/>
                    <Route path="/tablet" component={Tablet}/>
                    <Route path="/tv" component={Tele}/>
                </Switch>      
            </div>
       </BrowserRouter>
    );
  }
}

export default App;
